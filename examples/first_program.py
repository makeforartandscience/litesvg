from litesvg import LiteSvg, Rectangle,Text

svg = LiteSvg(width=500,height=200)

rect = Rectangle(x=50,width=400,y=50,height=100,rx=8)
rect.style.set(fill='#FFB030',stroke='#0060FF',stroke_width=10)
svg.add(rect)

text = Text(x=250,y=130,text='LITESVG')
text.style.set(font_size=80,font_weight='bold',text_anchor='middle',fill='#0060FF')
svg.add(text)

print(svg.render())

