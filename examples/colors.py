
from litesvg import LiteSvg, Rectangle

svg = LiteSvg(config='litesvg.json',width=400,height=400,y_up=True)

Rectangle.set_default(width=44,height=44,anchor='center',rx=8)

for j in range(8):
    for i in range(8):
        r = Rectangle(x=25+50*i,y=25+50*j)
        r.style.set(fill='#{:02X}{:02X}80'.format(32*i,32*j))
        svg.add(r)

svg.render('colors.svg')  
